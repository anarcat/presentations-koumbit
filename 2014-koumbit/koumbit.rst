Le Réseau Koumbit
=================

.. include:: <s5defs.txt>

Mission
-------

 * espace d'entraide et de partage de ressources pour les travailleurs
   des technologies de l'information engagés socialement dans leur
   milieu

 * favoriser l'appropriation de l'informatique libre et de favoriser
   l'autonomie technologique des groupes sociaux québécois, en
   développant une plateforme informatique collective et en assurant
   un support à l'utilisation des logiciels libres


Historique
----------

   * Origine du nom ("association de paysans en vue de réaliser des tâches communes, une forme de solidarité fondamentale dans la culture haïtienne", 1940, Jacques Roumain, militant/écrivant, "Gouverneurs de la Rosée")
   * CMAQ, fondation 2004
   * Base de clients, impossible de répondre bénévolement -> Coop
   * 3 générations: fondation (bénévoles + pigistes, 5-15, 50k-300k$), salariés (15-30, 300k-800k$), partenaires (~15, ~500k$)


Structure
---------

   * OBNL -> Coop
   * AG -> CA -> CT -> Comités
   * Comités: sysadmin, web, finances, RH, comcom, ventes, légal, femmes
   * Équipes de production (sysadmin/web)
   * Partenaires
   * Taux horaire unique
   * Pas de poste de patron à l'interne
   * Décisions prises par consensus actif

Principes fondateurs
--------------------

2004-2013

 * autogestion
 * espace pédagogique
 * transparence
 * copyleft
 * autosuffisance
 * solidarité
 * équité et égalité
 * parecon

Principes et valeurs
--------------------

~2014+

 * autogestion (décisions collectives, développement personnel, égalité)
 * ouverture ("libre", transparence, vie privée, liberté d'expression)
 * solidarité (réseau de partage, autonomie technologique, associations affinitaires)

Affaires
--------

   * 50% sysadmin, 50% web
   * projets de 5000-50 000$
   * web:
     * création et design de sites web
     * consultation et développement Drupal
     * accompagnement Wordpress et autres plateformes en développement
   * sysadmin:
     * HAG
     * Serveurs virtuels, dédiés, colo
     * Monitoring et support de veille
     * Sites à haut traffic et l'optimisation de performance
     * Réseau
     * Beaucoup de services en développement:
       * Aegir + AegirVPS
       * "Owncloud" - calendriers, partage de fichiers, tâches, contacts
       * Hébergement Redmine, git
       * Wikis

clients
-------

   * éducatif: McGill, UQAM, Concordia
   * commercial: JPR, Initia
   * OBNL: Alternatives, OXFAM-Québec, Isuma

.. sources:
.. https://wiki.koumbit.net/PrincipesEtValeurs
.. https://wiki.koumbit.net/Pr%C3%A9sentationDeKoumbit
.. presentations/ventes/2012-sqs/koumbit.html
.. https://wiki.koumbit.net/ServiceList
.. https://wiki.koumbit.net/PrincipesFondateurs
.. https://wiki.koumbit.net/RapportAnnuel2013?highlight=%28%5CbCat%C3%A9gorieRapportAnnuel%5Cb%29
.. https://wiki.koumbit.net/DocumentDePr%C3%A9sentation
.. https://wiki.koumbit.net/Historique
