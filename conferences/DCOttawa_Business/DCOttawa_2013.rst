Aegir-based Business Models
===========================

.. include:: <s5defs.txt>

.. class:: presenters

 Christopher Gervais & Guillaume Boudrias


Presenters
----------

Christopher Gervais | ergonlogic | chris@koumbit.org

Guillaume Boudrias | gboudrias | gboudrias@koumbit.org


Agenda
------

* What is the Aegir Hosting System?
* Aegir demo
* The Aegir Project
* Business models
* AegirVPS.net case-study
* Resources


What is the Aegir Hosting System?
---------------------------------
.. image:: ui/DCMunich_2012/aegir_logo.png
   :align: right

* Tools, functions & components
* Project, community & ecosystem

Tools & Functions
-----------------
.. image:: ui/DCMunich_2012/dries_portrait.jpg
   :align: right

**"Aegir is an important tool in the toolbox of the Drupal community."**

.. class:: right

-- *Dries Buytaert*


Tools & Functions
-----------------

* Aegir makes it easy to *install*, *upgrade*, *deploy*, and *backup* an entire network of Drupal sites.
* Complex, sophisticated system
* Essentially requires a dedicated server


Comparison
----------
.. image:: ui/DCMunich_2012/comparison.png
   :align: right

* Pantheon
* Acquia Cloud/Dev Desktop
* Shared/dedicated hosting

Aegir in Action
---------------

.. class:: huge

Demo time!

Project & Community
-------------------
.. image:: ui/DCMunich_2012/aegir_logo.png
   :align: right

* Active community: 5 maintainers, 20+ devs
* Only fully free open source software stack
* Helpful community_ and professional `service providers`_

.. _community: http://community.aegirproject.org
.. _`service providers`: http://community.aegirproject.org/service-providers

Ecosystem
---------
.. image:: ui/DCMunich_2012/devshops.png
   :align: right

* Development shops
* Consulting firms
* Hosting companies
* SaaS providers
* Clients & other users

Core Components
---------------
.. image:: ui/DCMunich_2012/components.png
   :align: right

* *Drush extension*: Provision_
* *Installation profile*: Hostmaster_
   * *Modules*: Hosting_
   * *Theme*: Eldir_

**N.B.** there is no "Aegir" project on drupal.org

.. _Provision: http://drupal.org/project/provision
.. _Hostmaster: http://drupal.org/project/hostmaster
.. _Hosting: http://drupalcode.org/project/hostmaster.git/tree/HEAD:/modules/hosting
.. _Eldir: http://drupalcode.org/project/hostmaster.git/tree/HEAD:/themes/eldir

Contrib Components
------------------
.. image:: ui/DCMunich_2012/contrib.png
   :align: right

* Active community: 20+ devs
* `Contrib modules`_: 35+
* Puppet: Aegir_ & Drush_
* Dev/test: Aegir-up_
* Makefiles: kPlatforms_

.. _`Contrib modules`: http://community.aegirproject.org/contrib-modules
.. _Aegir: http://drupal.org/project/puppet-aegir
.. _Drush: http://drupal.org/project/puppet-drush
.. _Aegir-up: http://drupal.org/project/aegir-up
.. _kPlatforms: http://drupal.org/project/kplatforms

Business models
---------------

.. class:: farright
.. image:: ui/DCMunich_2012/busmodel.png

* Infrastructure-as-a-Service
* Features Lab / Site Factory / Theme Studio
* Software-as-a-Service

*Honorable mention*: Platform-as-a-Service (PaaS)


Infrastructure-as-a-Service
---------------------------
.. class:: list-header

Description: Dedicated & managed Aegir servers

1. Automated deployments
2. Monitoring/support
3. Deep Aegir/sysadmin skillset
4. ????...
5. PROFIT!


Infrastructure-as-a-Service
---------------------------
.. class:: list-header

Description: Dedicated & managed Aegir servers

1. Automated deployments
2. Monitoring/support
3. Deep Aegir/sysadmin skillset
4. **Client-controlled configuration**
5. PROFIT!


Client config
-------------

.. class:: level

.. image:: ui/DCMunich_2012/AegirVPS-diagram.png
   :align: center


Lab / Factory / Studio
----------------------
.. class:: list-header

Description: Classic Drupal development shop

1. Skilled Drupal developers
2. Features/Git-based dev-test-staging workflow
3. ????...
4. PROFIT!


Lab / Factory / Studio
----------------------
.. class:: list-header

Description: Classic Drupal development shop

1. Skilled Drupal developers
2. Features/Git-based dev-test-staging workflow
3. **Self-hosted or Managed Aegir**
4. PROFIT!


Platform Maintenance
--------------------

.. image:: ui/DCMunich_2012/aegir_upg_workflow.png


Dev->Stage->Prod
----------------

.. image:: ui/DCMunich_2012/aegir_dev_workflow1.png


Software-as-a-Service
---------------------
.. class:: list-header

Description: Subscriptions to pre-built & updated sites

1. Install profiles
2. Aegir e-commerce integration
3. ????...
4. PROFIT!


Software-as-a-Service
---------------------
.. class:: list-header

Description: Subscriptions to pre-built & updated sites

1. Install profiles
2. Aegir e-commerce integration
3. **Managed user experience**
4. PROFIT!


Local Dev
---------

.. image:: ui/DCMunich_2012/saas_upg_workflow.png


AegirVPS.net case-study
-----------------------

.. sidebar:: Timeline

 | 2005 | Koumbit founded \n
 | 2006 | AlternC
 | 2008 | Aegir 0.1
 | 2011 | Aegir 1.0
 | 2013 | Aegir 2.0/3.0

.. class:: list-header

Current stats for aegir.koumbit.net:

* Active sites: 247
* Platforms: 21
* Clients: 136


AegirVPS.net case-study
-----------------------
.. class:: list-header

 **Client profile**: Mid-sized non-profit

* 1 production site, multiple dev sites
* Dedicated VM
* Managed Aegir
* Support contract

AegirVPS.net case-study
-----------------------
.. class:: list-header

 **Client profile**: Large Festival

* Multiple large sites
* Dedicated hardware, private VLAN, &c.
* Clustered DBs, file-servers, web-nodes
* Advanced Varnish caching
* 24x7 Support contract

AegirVPS.net case-study
-----------------------
.. class:: list-header

 **Client profile**: Freelance Site-builder

* 40+ small sites
* Dedicated VM
* Managed Aegir
* kPlatforms
* Support/consulting contracts

AegirVPS.net case-study
-----------------------
.. class:: list-header

 **Client profile**: Mid-sized Drupal Dev Shop

* 20+ medium sites, 8 devs
* Dedicated VMs (Dev, Prod, Varnish)
* Remote Import
* New Relic
* Managed Aegir

AegirVPS.net case-study
-----------------------
.. class:: list-header

 **Client profile**: SaaS/Dev Shop

* Dedicated AWS EC2 instances
* DevShop automated environments
* Vagrant-based local client
* Continuous delivery

Resources
---------
.. image:: ui/DCMunich_2012/aegir_logo.png
   :align: right

* api.aegirproject.org_
* `Issue queues`_
* #aegir on IRC
* `Aegir Service Providers`_
* community.aegirproject.org_:
   * Handbook, FAQ, Discussions

.. _community.aegirproject.org: http://community.aegirproject.org
.. _api.aegirproject.org: http://api.aegirproject.org/
.. _`Issue queues`: http://drupal.org/project/issues?text=&projects=provision,+hosting,+hostslave,+eldir,+Hostmaster+(Aegir)&status=Open&priorities=All&categories=All
.. _`Aegir Service Providers`: http://community.aegirproject.org/service-providers

Big news!
---------

.. sidebar:: Aegir 2 is coming!

  release date:

  First alphas are out

.. class:: normal

* Support for Drush 5
* Hosting queue runner, and other contrib features
* Subsite support (example.com/siteA)
* Improvements to support for nginx, SSL &c.

