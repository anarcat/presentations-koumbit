====================================================
L'infrastructure de l'Internet: propriété et pouvoir
====================================================

.. footer:: Koumbit 2008-2009 - copyleft

*Networking means connecting people to people and people to information; it
does not mean connecting computers to computers.*

 -- Wendy D. White, 1994, Growing the internet in Africa

D'où je viens
-------------

* résultat d'une réflexion "Koumbit dans 20 ans" et d'une présentation à Dijon
* sysadmin, programmeur, homme à tout faire... entrepreneur??
* utilisateur d'internet depuis ~1990
* militant du logiciel libre et du réseau libre

Théories et objectifs
---------------------

.. class:: small

1. La **marchandisation** des réseaux de communication laisse libre cours à la **privatisation du savoir**, au **contrôle et censure de l'information** et la **surveillance de masse** à des fins politiques et économiques
2. Il est possible de recréer des réseaux alternatifs, démocratiques, ouverts et accessibles

**But**: montrer la nécessité de travailler à l'élaboration de ce réseau et à la réappropriation des infrastructures par le communautaire et le public.

L'internet c'est...
-------------------

* des **réseaux interconnectés**, venu de:
* projet de l'armée américaine (**ARPAnet**)
* devenu projet **universitaire** (quelques réseaux)
* devenu projet **commercial** (beaucoup de réseaux!)
* devenu **infrastructure de communication globale** (beaucoup, beaucoup de réseaux!!)

Et quand je dis "beaucoup"
--------------------------

.. image:: isp-opte.png
   :scale: 70
   :align: center
   :target: isp-opte.png

`Source: Opte.org <http://opte.org/maps/>`_

L'opérateur réseau
------------------

* Personne ou organisation qui fait fonctionner un ou plusieurs réseaux
* FAIs: "Founisseurs d'Accès Internet", ils donnent accès à "Internet" (e.g. Bell, UQBM)
* Hébergeurs et autres fournisseurs (Koumbit, Communautique)

Un système autonome
-------------------

* Tous les réseaux sous la responsabilité d'un même opérateur
* Vu comme un seul réseau de l'extérieur, mais peut-être composé de plusieurs
* Minimum technique pour les ententes de peering et de transit multiples

"Connectivité globale"
----------------------

* L'internet est un réseau globalement connecté
* Être *sur internet* = joindre n'importe quelle machine
* Les routeurs centraux (*core routers*) doivent connaître tous les réseaux
* 276348 réseaux autonomes le 30 juillet 2008 (1420 réseaux IPv6)

Ça grossit très vite
--------------------

.. image:: bgp-active.png
   :align: center

`Source: bgp.potaroo.net <http://bgp.potaroo.net/>`_

Interconnexions
---------------

* L'Internet veut dire INTERconnected NETworks
* Ces réseaux sont connectés selon différentes ententes commerciales/politiques
* Aussi avec différentes technologies: dialup (oui oui!), ADSL, Ethernet, Fibre Optique, Wifi, Wimax... etc. etc.

Services de connectivité
------------------------

* Un opérateur vend une connexion à son réseau et tous les réseaux auquel il est relié
* Chargé au débit (*e.g. ~50$/mbps*) ou au total transféré ("burst", *<1$/gigabyte*) [*]_
* **Payant**
* Vous êtes ici.
* En anglais: "transit providers"

.. class:: tiny
.. [*] Prix connus par Koumbit à Montréal, prix beaucoup moins chers
   ailleurs...

Entente de réciprocité
----------------------

* Deux opérateurs interconnectent leur réseau physiquement
* Coûts d'interconnection et de maintenance partagés
* L'information circule librement dans les deux sens
* Peut sauver des frais de bande passante, surtout quand un FAI se branche sur un fournisseur de service
* **Gratuit**
* En anglais: "peering agreements"

Opérateurs "tier 3"
-------------------

* Seulement un fournisseur d'accès
* Exemples:

  * Votre connexion à la maison
  * Communautique (Vidéotron)
  * Koumbit (Netelligent)
  * La plupart des gens et organisations

Opérateurs "tier 2"
-------------------

* Ont plus d'une interconnexion (*transit* ou *réciprocité*)
* Vendent parfois aussi du *transit*
* Ont intérêt à faire du peering avec le plus de réseaux possibles
* La plupart des FAIs et plusieurs fournisseurs de service ont des réseau Tier 2
* Exemples:

  * Gitoyen (france)
  * Koumbit l'an prochain?
  * Bell... iweb... etc...

Opérateurs "tier 1"
-------------------

* Les opérateurs du "premier tiers" peuvent rejoindre n'importe quel point sans avoir à passer par un fournisseur de *transit*
* Donc *réciprocité* ("peering") avec tous les autres tier 1
* Oligarchie (AKA "racket") implicite:

.. class:: tiny

    1. AOL Transit Data Network (ATDN) (AS1668)
    2. AT&T (AS7018)
    3. Global Crossing (GX) (AS3549)
    4. Level 3 (AS3356)
    5. Verizon Business (AS701)
    6. Nippon Telegraph and Telephone Corp. (NTT) / Verio (AS2914)
    7. Qwest (AS209)
    8. SAVVIS (AS3561)
    9. Sprint Nextel Corporation (AS1239)

En résumé
---------

.. class:: tiny

Un exemple théorique:

.. image:: tiers.png
   :align: center 

.. class:: tiny

* connexions pas nécessairement représentatives
* il manque des réseaux

La "gouvernance"...
-------------------

* ICANN: assignation des "numéros" et des noms, répond au dpt. de
  commerce US (veto -> oversight)
* ISOC/IETF: standards de communications (les fameux RFCs)
* opérateurs réseaux (et leurs gestionnaires)
* administrateurs système (les travailleurEs, e.g. moi!)
* utilisateurs! (vous et moi!) (e.g. anonymous, zombie flashmobs, zapatista)

`C'est compliqué. <http://documentaliste.ac-rouen.fr/spip/gouvernance_internet/La%20gouvernance%20d%27Internet.html>`_

...et les menaces
-----------------

* sur les noms d'internet
* sur les nombres d'internet
* sur la neutralité d'internet

Les "noms" d'internet...
-------------------------

* ... les noms de domaines
* 14+ domaines "top level" généraux (gTLDs) et 243+ domaines "top level" "nationaux"
* L'ICANN a accrédité 789 registraires généraux, ouvert les gTLDs
* C'est "big": 70,000$ de capital
* Les TLDs locaux ont leur registraires locaux
* "13" serveurs pour répondre pour les "top level domains"

.. class:: small

  *VeriSign, ISI, Cogent, University of Maryland, NASA, ISC, U.S. DoD NIC,
  U.S. Army Research Lab, Autonomica, RIPE NCC, ICANN, WIDE Project*

"Je suis un voleur"
-------------------

* un problème derrière la marchandisation des noms
* privatisation d'Internic -> NSI == gratuit -> 50$, coût évalué 30¢/domaine
* il n'y a pas de raison technique pour ne pas avoir .mag, .bank, .coop (oups!)
* "une pénurie artificielle"

.. class:: small

`"Je suis un voleur. Je vends des noms de domaines. Je gagne beaucoup d'argent en vendant à un public qui n'y comprend rien un simple acte informatique qui consiste à ajouter une ligne dans une base de données."`

.. class:: tiny

    -- Laurent Chemla, co-fondateur de gandi.net http://www.chemla.org/textes/voleur.html

Les "nombres" d'internet
------------------------

* Les addresses IP (Internet Protocol)
* "*Autonomous System* Number" (ASN)
* Les allocations sont deleguées à 5 entitées régionales:

  * ARIN: Amérique du Nord
  * RIPE NCC: Europe
  * APNIC: Asie et le Pacifique
  * LACNIC: Amérique Latine et Caraïbes
  * AfriNIC: Afrique (*créé en 2005*)

L'autre pénurie
---------------

* L'équivalent de la pénurie des domaines avec les IPs
* Pu d'IPs d'ici 2010-2012
* Non-intentionnel, limitation technique ("640k should be enough for everyone")
* Voir le rapport de `Hurricane Electric <http://he.net/news/Hurricane_Electric_IPv6_Update_April_2008.pdf>`_, `XKCD <http://xkcd.com/195/>`_

Solution: IPv6
--------------

* règle le problème de façon permanente 
* voir `ce joli graphique <http://en.wikipedia.org/wiki/Image:Internet_address_spaces.svg>`_
* ... mais adoption lente par l'industrie et surtout les FAI
* conflit d'intérêt?
* adopté par: Linux (2.6.12/2005), BSD (2000), Microsoft (Vista/2007), Apple (Panther/2003, Airport/2007), IANA/DNS (2008), Google (2008) (`Source: Wikipedia <http://en.wikipedia.org/wiki/IPv6#Major_IPv6_announcements_and_availability>`_)

Neutralité des réseaux
----------------------

Network neutrality is best defined as a **network design principle**. The idea is that a maximally **useful public information network** aspires to **treat all content**, sites, and platforms **equally**. - Tim Wu

If I pay to connect to the Net with a given quality of service, and you pay to connect to the net with the same or higher quality of service, then you and I can communicate across the net, with that quality of service. - Tim Berners-Lee (inventeur du WWW)

Définition en 10 points
-----------------------

.. class:: small

 1. Nécessite un **transport commun** ("Common Carriage")
 2. A une **architecture ouverte** et supporte le développement de pilotes ouverts et libres de droits
 3. Est basé sur des **protocoles et des standards ouverts**
 4. Supporte l'architecture point à point i.e. conçu comme un **réseau non-intelligent** ("dumb")
 5. Est **privé** (e.g. pas de cheval de troie, d'inspection en profondeur des contenus, etc.)
 6. Est **neutre par rapport à l'application**
 7. À **basse latence** et basé sur une priorité de premier arrivé, premier servi (i.e. besoin d'une capacité suffisante)
 8. Est **interopérable**
 9. Est **neutre par rapport au modèle économique**
 10. Est **contrôlé par ses usagers** (i.e. est représenté internationalement et non-américano-centrique) 

.. class:: tiny

Définition de Sascha Meinrath and Victor W. Pickard, de l'Université du Michigan.

Les infractions
---------------

Certains ont d'autres intérêts ou du moins certainement d'autres politiques.

.. image:: hell.png
   :align: center


Infractions en Amérique
-----------------------

* **Technique**: Bell, Vidéotron interdisent de partager votre connexion et de faire des serveurs
* **Politiques**: Telus - censure d'un site web d'un syndicat rival
* **Anti-compétitives**: Shaw - réduisent la qualité du service pour compétiteurs
* **Sauvagement capitalistes**: Bell - "throttling" et surcharge de revendeurs et de clients

Étude de cas: Bell
------------------

.. image:: throttling-explained.png
   :align: center

.. class:: small

* CRTC approuve tacitement la tactique en "last resort"
* Conflit d'intérêt manifeste
* Australie: fibre publique
* http://competitivebroadband.com/

Infractions ailleurs
--------------------

* **Tunisie**: censure RSF durant le SMSI. Source: `anarcat.koumbit.org - Comment la Tunisie censure l'internet <http://anarcat.koumbit.org/2005-11-23-comment-la-tunisie-censure-linternet>`_
* **Chine**: a censuré wikipedia, conclu une entente pour la censure de Yahoo, Google, MSN, délation des bloggeurs par les corporations, abus généralisé
* **Beaucoup d'autres cas**, voir `RSF.org, section Internet <http://www.rsf.org/rubrique.php3?id_rubrique=273>`_
* surtout de la censure et surveillance, un autre exemple?

Étude de cas: USA
-----------------

.. image:: NSA_main.jpg
  :align: right

* **AT&T**: `Monopole refait <att-remerge.png>`_ veut charger pour utiliser *son* réseau
* **DCSNet**: FBI surveille tout communication téléphonique, fax ou cellulaire aux USA (1997-)
* **NSA**: surveillance de toutes les communications électroniques grâce à AT&T et autres (2001-)

Des solutions?
--------------

* devenir un opérateur réseau?
* faire notre propre FAI?
* faire notre propre réseau local ou même national?

Allons-y tranquillement...

Devenir opérateur réseau?
-------------------------

* Différents niveaux d'indépendance:

  * Client dans un centre de données non-neutre ou d'un service domiciliaire: totalement dépendant (*Tier 3*)
  * Plus d'un fournisseur de service: plus facile de déplacer les services et changer de fournisseurs (*Tier 2*)
  * Système autonome: peut avoir plusieurs présences sans changer d'adresses, changer de place sans problème... (*Tier 2*)
  * Accompli par Gitoyen en France

Faire un réseau local?
----------------------

* connexions entre les bureaux
* "City-wide WAN", AKA "MAN" (e.g. la pierre qu'on lance en inde, île sans fil, etc)
* Accompli par Reseau Citoyen à Bruxelles  (partage de données en local, geek devenu commun)

Faire un réseau national?
-------------------------

* ... voir international?
* interconnexion avec NYC, Toronto, Seattle... Paris!
* exemple: RISQ

.. image:: risq.png
   :scale: 50
   :align: center
   :target: risq.png

Faire notre propre FAI?
-----------------------

* plus qu'être autonome comme Koumbit
* partager l'autonomie avec tous
* Accompli par FDN en France

Étude de cas: UQBM
------------------

**Un Québec Branché sur le Monde**: Fournir à un **prix raisonnable**
l’Internet **haute vitesse** et les moyens d’en profiter à **tous**
les Québécois des régions rurales qui n’y ont pas encore accès

**Internet**, service **public**.

.. class:: small

Faire le plus grand chantier de connexion à Internet de l'histoire du
Québec, par les groupes communautaires, la collaboration de
l'entreprise privée et l'implication du gouvernement.

http://uqbm.qc.ca/

Pourquoi?
---------

* Canada passé de 2e à 30e en connectivité haute vitesse
* Bell: "normal" 2 mbps, "performance" 6, "max 12", "16" (20, 30, 40, 50$/mo)
* Moyenne de l'OCDE: 9mbps, 15 pour câble, Japon: 160mbps
* 3e plus cher (4$/m, après Mexique et et la Pologne, juste devant la Turquie. 0,07$ (Japon) 0,25$ (France), 0,34$ (corée))
* 6e plus lent (6mbps en moyenne, france: 50mbps)

.. class:: small

Sources: http://www.davidellis.ca/?p=802, `OCDE <http://www.oecd.org/sti/ict/broadband>`_

Pourquoi? (2)
-------------

* Argent!
* Indépendance accrue envers les fournisseurs privés
* Action directe pour la neutralité des réseaux, contre les fournisseurs hostiles
* Sécurité accrue
* (et pis c'est l'fun. :)

Le rêve...
----------

Un nouvel internet citoyen, libre, ouvert et neutre où tous aurait droit
d'accès équitablement et pourraient échanger librement. "Par et pour le
communautaire?"

Ce n'est pas si nouveau ou impossible, quand on y pense, on a déjà
commencé sans y penser!

Questions/discussions
---------------------

.. class:: tiny

Merci à lunar@anargeek.net et `PGA <http://agp.org/>`_ pour la
présentation sur laquelle je me suis basé pour produire celle-ci et sans
laquelle elle n'aurait pas été possible.

.. image:: logo-koumbit.png
   :align: center

.. class:: tiny

Slides produites avec `Docutils S5 <http://docutils.sourceforge.net/docs/user/slide-shows.html>`_ avec la commande: ``rst2s5 --theme default rst.txt rst.html`` Images de Tomato, le EFF, Bell Canada, etc.

.. class:: tiny

Texte disponible (et commentaires bienvenus!) sur http://wiki.koumbit.net/FormationInfrastructureInternet

.. vim: syntax=rst
