-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (x86_64)
--
-- Host: mysql.aegir.koumbit.net    Database: test2mvcsitesaeg
-- ------------------------------------------------------
-- Server version	5.1.63-0+squeeze1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrate_example_beer_node`
--

DROP TABLE IF EXISTS `migrate_example_beer_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrate_example_beer_node` (
  `bid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Beer ID.',
  `name` varchar(255) NOT NULL,
  `body` varchar(255) DEFAULT NULL COMMENT 'Full description of the beer.',
  `excerpt` varchar(255) DEFAULT NULL COMMENT 'Abstract for this beer.',
  `countries` varchar(255) DEFAULT NULL COMMENT 'Countries of origin. Multiple values, delimited by pipe',
  `aid` int(11) DEFAULT NULL COMMENT 'Account Id of the author.',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image path',
  `image_alt` varchar(255) DEFAULT NULL COMMENT 'Image ALT',
  `image_title` varchar(255) DEFAULT NULL COMMENT 'Image title',
  `image_description` varchar(255) DEFAULT NULL COMMENT 'Image description',
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=100000000 DEFAULT CHARSET=utf8 COMMENT='Beers of the world.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrate_example_beer_node`
--
-- ORDER BY:  `bid`

LOCK TABLES `migrate_example_beer_node` WRITE;
/*!40000 ALTER TABLE `migrate_example_beer_node` DISABLE KEYS */;
INSERT INTO `migrate_example_beer_node` VALUES (99999997,'Boddington','English occassionally get something right','A treat','United Kingdom',1,NULL,NULL,NULL,NULL);
INSERT INTO `migrate_example_beer_node` VALUES (99999998,'Miller Lite','We love Miller Brewing','Tasteless','USA|Canada',1,NULL,NULL,NULL,NULL);
INSERT INTO `migrate_example_beer_node` VALUES (99999999,'Heineken','Blab Blah Blah Green','Green','Netherlands|Belgium',0,'heineken.jpg','Heinekin alt','Heinekin title','Heinekin description');
/*!40000 ALTER TABLE `migrate_example_beer_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrate_example_beer_topic`
--

DROP TABLE IF EXISTS `migrate_example_beer_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrate_example_beer_topic` (
  `style` varchar(255) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `style_parent` varchar(255) DEFAULT NULL COMMENT 'Parent topic, if any',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region first associated with this style',
  `hoppiness` varchar(255) DEFAULT NULL COMMENT 'Relative hoppiness of the beer',
  PRIMARY KEY (`style`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrate_example_beer_topic`
--
-- ORDER BY:  `style`

LOCK TABLES `migrate_example_beer_topic` WRITE;
/*!40000 ALTER TABLE `migrate_example_beer_topic` DISABLE KEYS */;
INSERT INTO `migrate_example_beer_topic` VALUES ('ale','traditional',NULL,'Medieval British Isles','Medium');
INSERT INTO `migrate_example_beer_topic` VALUES ('pilsner','refreshing',NULL,'Pilsen, Bohemia (now Czech Republic)','Low');
INSERT INTO `migrate_example_beer_topic` VALUES ('red ale','colorful','ale',NULL,NULL);
/*!40000 ALTER TABLE `migrate_example_beer_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrate_example_beer_topic_node`
--

DROP TABLE IF EXISTS `migrate_example_beer_topic_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrate_example_beer_topic_node` (
  `bid` int(11) NOT NULL COMMENT 'Beer ID.',
  `style` varchar(255) NOT NULL COMMENT 'Topic name',
  PRIMARY KEY (`style`,`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Beers topic pairs.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrate_example_beer_topic_node`
--
-- ORDER BY:  `style`,`bid`

LOCK TABLES `migrate_example_beer_topic_node` WRITE;
/*!40000 ALTER TABLE `migrate_example_beer_topic_node` DISABLE KEYS */;
INSERT INTO `migrate_example_beer_topic_node` VALUES (99999999,'pilsner');
INSERT INTO `migrate_example_beer_topic_node` VALUES (99999998,'red ale');
INSERT INTO `migrate_example_beer_topic_node` VALUES (99999999,'red ale');
/*!40000 ALTER TABLE `migrate_example_beer_topic_node` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-10-12 10:59:14
