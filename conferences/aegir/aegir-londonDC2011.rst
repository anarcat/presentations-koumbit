Aegir at Drupalcon London 2011
==============================

.. include:: <s5defs.txt>

Splash
------

.. image:: ui/medium-aegir/splash-aegir.png

What is Ægir?
-------------

Norse god of Drupal. Ask the Danes!

But it's spelled "Aegir" because we're atheists. ;)

Use cases
---------

* Mass hosting
* Handling upgrades easily
* Automated testing
* Dev -> staging -> live

Houston we have 1.0!
--------------------

We are 1.0 - no wait, 1.2!

* 24 dev releases
* 3 stable releases

Crazy, isn't it?

What's in it?
-------------

* SSL support - generates that CSR!
* batch migrations
* renames - fixes sites/default
* cloning sites - template sites!
* remote DB and web servers

Big news
--------

Things have improved - even YOU can install Aegir!

Before
------

Follow this text file!

http://drupalcode.org/project/provision.git/blob_plain/provision-0.4-rc1:/docs/INSTALL.txt

After
-----

apt-get install aegir

or follow the cute install instructions:

http://community.aegirproject.org/installing

Live demos never fail
---------------------

Let's try to prove this again.

Our policies
------------

* Tools, not policy
* Release early, release often
* Easy to install
* Community-friendly (d.o, newbie-friendly)

We're solid
-----------

* Stable trunk
* Eat our own dogfood
* Continuous integration (Jenkins!)
* 1.0 led to ... 


Tons of contrib!
----------------

* queue daemon
* ubercart integration
* boost.module support
* civicrm support
* ACL supports

Where's yours?!
---------------

community.aegirproject.org/contrib

What's next? 2.0!
-----------------

* rock solid
* port to Drupal 7 / Drush 5
* performance improvements (daemon)
* more pluggability...

which means...

WORDPRESS SUPPORT!
------------------

Heresy! Scandal! Drama!

Thanks
------

Questions? Feedback?
