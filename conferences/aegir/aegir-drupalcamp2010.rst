Ægir Drupalcamp Montreal 2010
=============================

.. include:: <s5defs.txt>

Splash
------

.. image:: ui/medium-aegir/splash-aegir.png

What is Ægir?
-------------

More importantly, how do we pronounce it?

Use cases
---------

* Mass hosting
* Dev -> staging -> live
* Handling upgrades easily
* Automated testing

.. create a d6 platform
.. create a site - dcmtldemo.test.koumbit.net
.. voila. login and al.
.. create d7 platform on new server
.. migrate to new server - dcdemo.alternd.com (doesn't upgrade your code!)

Progress since 0.3
------------------

.. class:: small
.. class:: center

Drush 3.0 integration (aliases), rewritten OO backend, refactored UI,
server/service abstraction, JS workflow improvements, remote DB
servers, remote web servers, web clusters, OpenSSL support, cloning
sites, renaming sites, migration between servers, DNS support...

The future
----------

.. default-role:: incremental
.. class:: huge
.. class:: center

`1.0!`

.. class:: tiny
.. class:: center

`(mentally add twinkles here)`

Plans for 1.0?
--------------

* We've ran out of roadmap!
* Stable, documented API
* Drupal 7 based
* Usability and workflow improvements
* Automated platform management
* Ubercart & contrib expansion

Q&A
---

Don't miss the other Ægir tomorrow same time:

.. class:: tiny
.. parsed-literal::

  drush_make('business models based on
              Ægir-Ubercart integration');

