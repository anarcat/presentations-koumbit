==================================
Aegir: one Drupal to rule them all
==================================

Overview
--------

* Using the Aegir hosting system (demo and overview)
* Security and defensive programming
* Digging into the design of Aegir
* Extending Aegir
* The Roamdp

Using the Aegir hosting system
------------------------------

 * install
 * backup/restore
 * migrate/clone
 * profiles are applications
 * sites are instances

Digging into the design of Aegir
--------------------------------

 * simplicity, ease of use and install
 * the frontend, the backend...
   * Drush, which includes aegir code
   * little history of drush
 * frontend/backend communication (json, pipes)
 * Distributed nature - fileservers
 * "features"

A few words about security and defensive programming
----------------------------------------------------

 * privilege seperation between frontend and backend
 * least necessary privilege
 * SSH for inter-server communications
 * Staged upgrades, no inline updating
 * Rollbacks. always provide a way back to a previously working system.

Extending and whiteboxing Aegir
-------------------------------

 * build your own hosted service
 * pop in your own theme
 * different features for different business cases

The Aegir roadmap
-----------------

 * 0.1 "sites" - basic sites management features
 * 0.2 "upgrades" - multi-platform suport
 * 0.3 "d6" - frontend drupal 6 support and stabilisation
 * 0.4 "cloud" - multi-server, DNS, clone and SSL, UI improvements
 * 0.5 "apt-get" - proper deployment procedures (features? proper project managemnet? etc)
 * 1.0 "api" - API freeze for long term development of third party modules

Questions?
----------

.. class:: tiny

.. image:: logo-koumbit.png
   :align: center

.. class:: tiny

Slides produced with `Docutils S5
<http://docutils.sourceforge.net/docs/user/slide-shows.html>`_ . Source
available at: https://svn.koumbit.net//koumbit/trunk/doc/aegir/presentation.html

.. vim: syntax=rst
