Drupal-to-Drupal Data Migration
===============================

.. include:: <s5defs.txt>

Matt Corks | mvc | matt@koumbit.org

Agenda
------

- A quick history of Drupal & data migration
- Why to use the migrate module, in case you're not yet convinced
- The magical migrate wizard
- Getting your hands dirty
- Questions

A quick history
---------------

- Let's just write this one quick script!
- Let's just use CSV! (Node Import)
- Let's just scrape other people's feeds! (FeedAPI; Feed Element Mapper)
- Let's just pull once from CSV or XML! (Feeds)
- Let's just provide lots of hooks! (Migrate 1.x)

Why to use the migrate module
-----------------------------

- Field mapping can be done via web frontend
- Customization assumes experience with object-oriented programming
- Data imports run from web frontend or via drush
- Core's upgrade path as of D8 (``update.php`` considered harmful)

Creating a migrate class
------------------------

- Define source & target entities, plus field mappings
- Alter data or skip rows in ``prepareRow()``
- Fetch additional data ``query()``
- Alter entities before they're saved in ``prepare()``
- Alter entities after they're saved in ``complete()``

Creating a migrate class
------------------------

- Specify dependencies (e.g., users before nodes)
- Mark unmapped source and destination fields
- Reference previously mapped fields
- Create placeholder entities (stubs) to allow for circular references

Learning to use migrate
-----------------------

- Migrate handbook: http://drupal.org/migrate
- Migrate D2D handbook: https://www.drupal.org/node/1813498
- `Migrate D2D example module`_

.. _Migrate D2D example module: http://cgit.drupalcode.org/migrate_d2d/tree/migrate_d2d_example

Migrate D2D wizard demo
-----------------------

- Drupal 6 to Drupal 7 migration

Migrate D2D code
----------------

- Migrating text fields to term or node references
- Migrating location module data to addressfield
- Migrating data into field collections
- Migrating a node reference which points to multiple types
- Migrating OG 6.x-1.x to 7.x-2.x

Questions?
----------

Slides
~~~~~~

 - http://mvc.koumbit.org/presentations/2014-drupalcampmtl/
 - `Sample code`_

 .. _Sample code: http://mvc.koumbit.org/presentations/2014-drupalcampmtl/migrate_stuff.tar.gz

